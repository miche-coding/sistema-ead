<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Course extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected  $table = 'course';


    protected $fillable = [
        'name','users_id','image','description','price','status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         '','_method','_token',
    ];

    public function user(){
        //relacionamento 1 para 1
        return $this->hasOne(User::class,'id');
    }
    public function courseAll() {

        $result = DB::select('SELECT 
                u.name as  user_name , c.name, c.price ,c.id ,c.status 
            FROM 
                course c , users u 
            WHERE 
                c.users_id = u.id');

      return  $result;
    }

   
}
