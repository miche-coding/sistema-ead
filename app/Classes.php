<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Classes extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected  $table = 'classes';


    protected $fillable = [
        'name','duration','modules_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         '_method','_token',
    ];

    public function modules(){
        //relacionamento 1 para 1
        return $this->hasOne(Modules::class,'id');
    }

    public function classesAll() {

        $result = DB::select('SELECT 
                        m.name as name_module, c.name, c.duration ,c.id 
                    FROM 
                        modules m , classes c 
                    WHERE 
                        m.id = c.modules_id');

        return $result;
    }
}
