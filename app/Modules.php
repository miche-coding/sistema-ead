<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Modules extends Model
{
      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected  $table = 'modules';


    protected $fillable = [
        'name','course_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         '_method','_token',
    ];

    public function course(){
        //relacionamento 1 para 1
        return $this->hasOne(Course::class,'id');
    }

    public function classes()
    {
        return $this->hasMany(Classes::class, 'modules_id', 'id');
    }

    public function modulesAll() {

        $result = DB::select('SELECT 
                m.name , m.id, c.name as name_course 
            FROM 
                modules m , course c 
            WHERE 
                m.course_id = c.id');

                return $result;
    }
}
