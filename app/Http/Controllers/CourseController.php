<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\CourseRequest;
use Illuminate\Support\Facades\Session;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Course $course)
    {
        $courses = $course->courseAll();

        return view('courses.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(User $users)
    {
        $users = $users->where('profile_id',1)->get();

        return view('courses.create',compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CourseRequest $request,Course $course)
    {
        $input = $request->except(['_method','_token']);

        if($input['users_id']==0){
            return \Redirect::back()->with('message',' Você precisa selecionar um author.');
        }

        $source = array('.', ',');

        $replace = array('', '.');

        $input['price'] = str_replace($source, $replace,$input['price']); //remove os pontos e substitui a virgula pelo ponto

        $course->create( $input);

        Session::flash('success', 'Curso adicionado com Sucesso!');

        return redirect()->action('CourseController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::find($id);

        $user = $course->user()->first();

        return view('courses.show',compact('course','user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course,User $users)
    {
        $user= $users->where('profile_id',1)->get();

        return view('courses.edit',compact('course','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(CourseRequest $request,Course $course)
    {
        $input = $request->except(['_method','_token']);

        if($input['users_id']==0){
            return \Redirect::back()->with('message',' Você precisa selecionar um author.');
        }

        $source = array('.', ',');

        $replace = array('', '.');

        $input['price'] = str_replace($source, $replace,$input['price']); //remove os pontos e substitui a virgula pelo ponto

        $course->update($input);

        return redirect()->action('CourseController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Course $course)
    {
        $course->delete();

        return redirect()->action('CourseController@index');
    }
}
