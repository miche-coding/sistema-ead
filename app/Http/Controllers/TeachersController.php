<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\TeachersRequest;
use Illuminate\Support\Facades\Session;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        $teachers = User::where('profile_id',1)->get();
        return view('teachers.index',compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeachersRequest $request)
    {  
        User::create([
            'name' => $request->get('name'),
            'email' =>$request->get('email'),
            'password' => bcrypt($request->get('password')),
            'profile_id' => 1,
        ]);

        Session::flash('success', 'Professor adicionado com Sucesso!');

        return redirect()->action('TeachersController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function show(User $teachers,$id)
    {
        $teacher = $teachers->find($id);

        return view('teachers.show',compact('teacher'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function edit(User $teachers,$id)
    {
        $teacher = $teachers->find($id);

        return view('teachers.edit',compact('teacher'));
        
        return redirect()->action('TeachersController@index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function update(TeachersRequest $request, User $teachers,$id)
    {
        $input = $request->except(['_method','_token']);

        $teachers->where('id',$id)->update($input);

        return redirect()->action('TeachersController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $teachers,$id)
    {
        User::where('id',$id)->delete();

        return redirect()->action('TeachersController@index');
    }
}
