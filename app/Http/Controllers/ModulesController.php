<?php

namespace App\Http\Controllers;

use App\Modules;
use App\Course;
use Illuminate\Http\Request;
use App\Http\Requests\ModuleRequest;
use Illuminate\Support\Facades\Session;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Modules $modules)
    {
        $modules = $modules->modulesAll();
        return view('modules.index',compact('modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Course $courses)
    {
        $courses = $courses->where('status',1)->get();

        return view('modules.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModuleRequest $request,Modules $modules)
    {

        if($input['course_id'] ==0){
            return \Redirect::back()->with('message',' Você precisa selecionar um curso.');
        }

        $modules->create($request->all());

        Session::flash('success', 'Curso adicionado com Sucesso!');

        return redirect()->action('ModulesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Modules  $modules
     * @return \Illuminate\Http\Response
     */
    public function show($id,Modules $modules)
    {

        $modules = $modules->find($id);

        $courses = $modules->course()->first();

        $classes = $modules->classes()->get();
        
        return view('modules.show',compact('modules','courses','classes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Modules  $modules
     * @return \Illuminate\Http\Response
     */
    public function edit(Modules $modules,Course $courses,$id)
    {
        $courses = $courses->where('status',1)->get();
        $modules = $modules->find($id);
        return view('modules.edit',compact('modules','courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Modules  $modules
     * @return \Illuminate\Http\Response
     */
    public function update(ModuleRequest $request,Modules $modules,$id)
    {
        $input = $request->except(['_method','_token']);

        if($input['course_id'] ==0){
            return \Redirect::back()->with('message',' Você precisa selecionar um curso.');
           
        }

        $modules->where('id',$id)->update($input);

        Session::flash('success', 'Módulo adicionado com Sucesso!');

        return redirect()->action('ModulesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Modules  $modules
     * @return \Illuminate\Http\Response
     */
    public function destroy(Modules $modules)
    {
        $modules->delete();

        return redirect()->action('ModulesController@index');
    }
}
