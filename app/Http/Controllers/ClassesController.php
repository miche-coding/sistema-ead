<?php

namespace App\Http\Controllers;

use App\Classes;
use Illuminate\Http\Request;
use App\Modules;
use App\Http\Requests\ClassesRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
class ClassesController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Classes $classes,Modules $modules)
    {
       $classes = $classes->classesAll();

        return view('classes.index',compact('classes','modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Modules $modules)
    {
        $modules = $modules->get();

        return view('classes.create',compact('modules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClassesRequest $request,Classes $classes)
    {
        
        if($request->get('modules_id')==0){
            return \Redirect::back()->with('message',' Você precisa selecionar um módulo.');
        }

        $classes->create($request->all());

        Session::flash('success', 'Aula criada com sucesso !');

        return redirect()->action('ClassesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function show($id,Classes $classes)
    {

        $classes = $classes->find($id);

        $modules = $classes->modules()->first();

        return view('classes.show',compact('classes','modules'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function edit($id,Classes $classes,Modules $modules )
    {
    
        $classes = $classes->find($id);

        $modules = $modules->get();

        return view('classes.edit',compact('classes','modules'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function update($id,ClassesRequest $request,Classes $classes)
    {

        $input = $request->except(['_method','_token']);

        if($input['modules_id']==0){
            return \Redirect::back()->with('message',' Você precisa selecionar um módulo.');
        }

        $classes->where('id',$id)->update( $input);

        Session::flash('success', 'Atualizado com sucesso !');

        return redirect()->action('ClassesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Classes $classes)
    {
        $classes->where('id',$id)->delete();

        return redirect()->action('ClassesController@index');
    }
}
