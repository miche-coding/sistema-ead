<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        switch($this->method()){
            case 'GET':
            case 'DELETE':{

            }

            case 'POST': {
                return [
                    'name' => 'required|max:100',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required',
                ];

            }

            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'required|max:100',
                    'email' => 'required',
                  
                ];

            }

        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Campo Nome deve ser preenchido.',
            'email.required' => 'Campo E-mail deve ser preenchido',
            'password.required' => 'Campo Senha deve ser preenchido',
            'email.unique' => 'E-mail já cadastrado no sistema, por favor digite outro',
        ];
    }
}
