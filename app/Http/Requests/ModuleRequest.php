<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        switch($this->method()){
            case 'GET':
            case 'DELETE':{

            }

            case 'POST': {
                return [
                    'name' => 'required|max:100',
                    'course_id' => 'required',

                ];

            }

            case 'PUT':
            case 'PATCH': {
                return [
                    'name' => 'required|max:100',
                    'course_id' => 'required',
                  
                ];

            }

        }
    }

    public function messages()
    {
        return [
            'name.required' => 'Campo Nome deve ser preenchido.',
            'course_id.required' => 'Selecione um Curso',
        ];
    }
}
