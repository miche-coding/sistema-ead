@extends('layouts.app')

@section('content')

<div class="box">

    <div class="box-header">
        <h3 class="box-title">Alterar dados do aluno</h3>
    </div>

    {!! Form::open(['url' =>  route('students.update',$student->id), 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'accept-charset' => 'utf-8']) !!}

        <div class="box-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Nome') !!}
                            {!! Form::text('name', $student->name, ['required', 'class' => 'form-control', 'placeholder'=>'Nome do aluno']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('E-mail') !!}
                            {!! Form::text('email',  $student->email, ['required', 'class' => 'form-control', 'placeholder'=>'Digite o e-mail. Ex: exemplo@exemplo.com']) !!}
                        </div>
                    </div>
                </div>
               
            </div>

            <div class="form-group text-right">
                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary bgpersonalizado']) !!}
            </div>
        </div>

    {!! Form::close() !!}

</div>

@endsection