@extends('layouts.app')

@section('content')

<div class="box">

    <div class="box-header">
        <h3 class="box-title">Dados do professor</h3>
    </div>

    {!! Form::open(['url' => 'reports', 'method' => 'post', 'enctype' => 'multipart/form-data', 'accept-charset' => 'utf-8']) !!}

        <div class="box-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Nome') !!}:
                            <span>{{ $student->name }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('E-mail') !!}:
                            <span>{{ $student->email }}</span>
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="form-group text-right">
                <a href="{{ url('students/') }}" class="btn btn-primary bgpersonalizado">Voltar</a>
            </div>
        </div>

    {!! Form::close() !!}

</div>

@endsection