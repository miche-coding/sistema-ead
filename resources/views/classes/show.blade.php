@extends('layouts.app')

@section('content')

<div class="box">

    <div class="box-header">
        <h3 class="box-title">Dados da aula</h3>
    </div>

        <div class="box-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Nome') !!}:
                            <span>{{ $classes->name }}</span>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Módulo') !!}:
                            <span>{{ $modules->name }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Duração') !!}: 
                            <span>{{ $classes->duration }}</span>
                        </div>
                    </div>
                </div>
                
            </div>

        </div>



</div>

@endsection