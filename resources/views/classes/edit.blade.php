@extends('layouts.app')

@section('content')

<div class="box">

    <div class="box-header">
        <h3 class="box-title">Alterar dados da aula</h3>
    </div>

    {!! Form::open(['url' => route('classes.update',$classes->id), 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'accept-charset' => 'utf-8']) !!}

        <div class="box-body">
            @if(count($errors)>0)

            <div class="alert alert-danger" role="alert">  
                <ul>
                    @foreach($errors->all() as $message)
                        <li>{{$message}}</li>	
                    @endforeach
                <ul>	
            </div>
            
            @endif
            @if (Session::has('message'))
            <div class="alert alert-danger">
               <?php echo Session::get('message')?>
            </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Nome categoria') !!}
                            {!! Form::text('name', $classes->name, ['required', 'class' => 'form-control', 'placeholder'=>'Nome da categoria']) !!}
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Módulo') !!}
                            <select required="" class="js-example-basic-single form-control" name="modules_id">
                                <option selected="selected" value="0">Selecione uma opção</option>
                                @foreach($modules as $module)
                                    <option  {{ $module->id ==  $classes->modules_id ? 'selected' : '' }}  value="{{  $module->id }}">{{  $module->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Duração') !!}
                            {!! Form::time('duration', $classes->duration, ['required', 'class' => 'form-control','placeholder'=>'Duração da aula']) !!}
                        </div>
                    </div>
                </div>
                
            </div>

            <div class="form-group text-right">
                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary bgpersonalizado']) !!}
            </div>
        </div>

    {!! Form::close() !!}

</div>

@endsection