@extends('layouts.app')

@section('content')

<div class="box">

    <div class="box-header">
        <h3 class="box-title">Alterar dados do módulo</h3>
    </div>

    {!! Form::open(['url' => route('modules.update',$modules->id), 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'accept-charset' => 'utf-8']) !!}

        <div class="box-body">
            @if(count($errors)>0)

            <div class="alert alert-danger" role="alert">  
                <ul>
                    @foreach($errors->all() as $message)
                        <li>{{$message}}</li>	
                    @endforeach
                <ul>	
            </div>
     
            @endif

            @if (Session::has('message'))
            <div class="alert alert-danger">
               <?php echo Session::get('message')?>
            </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Nome') !!}
                            {!! Form::text('name', $modules->name, ['required', 'class' => 'form-control', 'placeholder'=>'Nome do módulo']) !!}
                        </div>
                    </div>
                </div>
                
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Curso') !!}
                            <select required="required" class="js-example-basic-single form-control" name="course_id">
                                <option selected="selected" valye="0">Selecione uma opção</option>
                                @foreach($courses as $course)
                                    <option {{ $course->id ==  $modules->course_id ? 'selected' : '' }} value="{{  $course->id }}">{{  $course->name }}</option>
                                @endforeach
                            </select>
                         
                        </div>
                    </div>

           

            <div class="form-group text-right">
                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary bgpersonalizado']) !!}
            </div>
        </div>

    {!! Form::close() !!}

</div>

@endsection