@extends('layouts.app')

@section('content')

<div class="box">

    <div class="box-header">
        <h3 class="box-title">Dados do módulo</h3>
    </div>

    

        <div class="box-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Nome') !!}:
                            <span>{{ $modules->name }}</span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Curso') !!}:
                            <span>{{ $courses->name }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Cronograma') !!}:
                            <ul>
                                @for($i=0; $i < count($classes) ;$i++ )
                                    <li>{{  $classes[$i]->name }}</li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>


</div>

@endsection