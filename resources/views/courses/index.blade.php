@extends('layouts.app')

@section('content')

<div class="box">
    
    <div class="box-header">
        <h3 class="box-title">Cursos</h3>
    </div>

    <div class="box-body table-responsive">

        <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Nome</th>
               
                    <th>Preço</th>
                   
                    <th>Status</th>
                    <th>Autor do curso</th>
                    <th>Ações</th>
                </tr>
            </thead>
 
            <tbody>
                
                @foreach($courses as $course)

                <tr>

                    <td> {{$course->name}} </td>
                    
                   
                    
                    <td>R$ {{number_format($course->price,2,',','.')}} </td>
                        

                    <td> 
                        @if($course->status == 1)
                            <label class="label label-success">Ativo</label>
                        @else 
                             <label class="label label-danger">Inativo</label>
                        @endif
                    </td>

                    <td> {{$course->user_name}} </td>
                


                    <td>

                        <a href="{{ URL('/') }}/courses/{{$course->id}}/edit" alt="Editar" title="Editar" class="btn btn-default">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>

                        <a href="{{ URL('/') }}/courses/{{$course->id}}" alt="Visualizar" title="Visualizar" class="btn btn-default">
                            <span class="glyphicon glyphicon-share-alt"></span>
                        </a>

                        <form method="POST" action="{{ route('courses.destroy', $course->id) }}" accept-charset="UTF-8">
                            {!! csrf_field() !!}
                            {!! method_field('DELETE') !!}
                            <button type="submit" onclick="return confirm('Tem certeza que quer deletar?')" class="btn btn-danger glyphicon glyphicon-trash">
                        </form>

                    </td>

                </tr>

                @endforeach

            </tbody>  

        </table>

        <br><br>

        <div class="form-group text-right">
            <a href="{{ URL('/') }}/courses/create" class="btn btn-primary bgpersonalizado">Cadastrar</a>
        </div>
        
    </div>

</div>

@endsection