@extends('layouts.app')

@section('content')

<div class="box">

    <div class="box-header">
        <h3 class="box-title">Dados do curso</h3>
    </div>

   

        <div class="box-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Nome') !!}:
                            <span>{{ $course->name }}</span>
                        </div>
                    </div>
                </div>
              
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Preço') !!}:
                            <span>R$ {{number_format($course->price,2,',','.')}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Status') !!}: 
                            <span>  
                            @if($course->status == 1)
                                <label class="label label-success">Ativo</label>
                            @else 
                                <label class="label label-danger">Inativo</label>
                            @endif</span>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Descrição') !!}:
                            <span class="title">{{ $course->description }}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group text-right">
                <a href="{{ url('courses/') }}" class="btn btn-primary bgpersonalizado">Voltar</a>
            </div>
        </div>



</div>

@endsection