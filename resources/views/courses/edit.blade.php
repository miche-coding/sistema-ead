@extends('layouts.app')

@section('content')

<div class="box">

    <div class="box-header">
        <h3 class="box-title">Alterar curso</h3>
    </div>

    {!! Form::open(['url' =>  route('courses.update',$course->id), 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'accept-charset' => 'utf-8']) !!}

        <div class="box-body">
            @if(count($errors)>0)

            <div class="alert alert-danger" role="alert">  
                <ul>
                    @foreach($errors->all() as $message)
                        <li>{{$message}}</li>	
                    @endforeach
                <ul>	
            </div>
            
            @endif
            @if (Session::has('message'))
            <div class="alert alert-danger">
               <?php echo Session::get('message')?>
            </div>
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Nome') !!}
                            {!! Form::text('name', $course->name, ['required', 'class' => 'form-control', 'placeholder'=>'Nome do curso']) !!}
                        </div>
                    </div>
                </div>
                
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Preço') !!}
                            {!! Form::text('price', $course->price, ['required', 'class' => 'form-control','placeholder'=>'Valor do curso. Ex: 0,00']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Status') !!}
                            {!! Form::select('status', [""=>"Selecione uma opção" , '1' => "Ativo", '2' => "Inativo"], $course->status, ['required', 'class' => 'js-example-basic-single form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('author', 'Autor do curso') !!}
                            <select required="" class="js-example-basic-single form-control" name="users_id">
                            <option selected="selected" value="0">Selecione uma opção</option>
                                @foreach($user as $u)
                                    <option {{ $u->id ==  $course->users_id ? 'selected' : '' }}  value="{{  $u->id }}">{{  $u->name }}</option>
                                @endforeach
                            </select>    
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Descrição') !!}
                            {!! Form::textarea('description', $course->description, ['required', 'class' => 'form-control','placeholder'=>'Escreva uma descrição do curso']) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group text-right">
                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary bgpersonalizado']) !!}
            </div>
        </div>

    {!! Form::close() !!}

</div>

@endsection