<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Sistema EAD </title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/datepicker.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/dataTables.tableTools.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>

</head>

<body class="skin-blue">
    <header class="header">
        <a href="{{ URL('/') }}" class="logo bgpersonalizado">Sistema EAD</a>
        <nav class="navbar navbar-static-top bgpersonalizado" role="navigation">
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" >{{ csrf_field() }}
                           <button style="margin-top:10px;" class="btn btn-primary" type="submit">SAIR</button>
                        </form>
          
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                <ul id="menu" class="sidebar-menu">


                    <li id='users'>
                        <a href="{{ URL('/') }}/teachers">
                            <i class="glyphicon glyphicon-list"></i>
                            <span>Cadastrar Professor</span>
                        </a>
                    </li>

                    <li id='profiles'>
                        <a href="{{ URL('/') }}/students">
                            <i class="glyphicon glyphicon-list"></i>
                            <span>Cadastrar  Aluno</span>
                        </a>
                    </li>

                    
                    <li id="course">
                        <a href="{{ URL('/') }}/courses">
                            <i class="glyphicon glyphicon-list"></i>
                            <span>Cadastrar Curso</span>
                        </a>
                    </li>

                    <li id='indicators'>
                        <a href="{{ URL('/') }}/modules">
                            <i class="glyphicon glyphicon-list"></i>
                            <span>Cadastrar Módulo</span>
                        </a>
                    </li>

                    <li id='logs'>
                        <a href="{{ URL('/') }}/classes">
                            <i class="glyphicon glyphicon-list"></i>
                            <span>Cadastrar Aulas</span>
                        </a>
                    </li>
                </ul>
            </section>
        </aside>

        <aside class="right-side">

            @if(Session::has('flash_success'))
                <div class="pad margin no-print">
                    <div class="alert alert-success" style="margin-bottom: 0!important;">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        <i class="icon fa fa-check"></i>
                        <b>{{ Session::get('flash_success') }}</b>
                    </div>
                </div>
            @endif

            @if(Session::has('flash_error'))
                <div class="pad margin no-print">
                    <div class="alert alert-danger" style="margin-bottom: 0!important;">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                        <i class="icon fa fa-check"></i>
                        <b>{{ Session::get('flash_error') }}</b>
                    </div>
                </div>
            @endif

            <section class="content">

                @yield('content')

            </section>

        </aside>

    </div>

    <footer class="footer">© 2020 - Todos os direitos reservados</footer>

    <script type="text/javascript" src="{{ asset('js/jquery-2.0.3.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/tinymce.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/jquery.fancybox.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/jquery.maskMoney.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/chosen.jquery.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/jquery.fancybox.js') }}"></script>

    <script type="text/javascript" src="{{ asset('js/dataTables.tableTools.min.js') }}"></script>
    
    <script type="text/javascript">
        
        var base = "{{ URL('/') }}";
        var controller = "{{ explode('/', Route::current()->uri)[0] }}";

    </script>
      <script type="text/javascript">
        $(document).ready(function(){
              $(".price").maskMoney({showSymbol:true, symbol:"R$", decimal:",", thousands:"."});
        });
    </script>
    <script src="{{ asset('js/rizer.js') }}"></script>

</body>
</html>
