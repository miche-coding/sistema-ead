@extends('layouts.app')

@section('content')

<div class="box">

    <div class="box-header">
        <h3 class="box-title">Dados do professor</h3>
    </div>

   

        <div class="box-body">

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Nome') !!}:
                            <span>{{ $teacher->name }}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('E-mail') !!}:
                            <span>{{ $teacher->email }}</span>
                        </div>
                    </div>
                </div>
                
              
            </div>

            <div class="form-group text-right">
                <a href="{{ url('teachers/') }}" class="btn btn-primary bgpersonalizado">Voltar</a>
            </div>
        </div>



</div>

@endsection