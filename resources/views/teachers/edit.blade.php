@extends('layouts.app')

@section('content')

<div class="box">

    <div class="box-header">
        <h3 class="box-title">Alterar dados do professor</h3>
    </div>

    {!! Form::open(['url' => route('teachers.update',$teacher->id) , 'method' => 'PUT', 'enctype' => 'multipart/form-data', 'accept-charset' => 'utf-8']) !!}

        <div class="box-body">
            @if(count($errors)>0)

            <div class="alert alert-danger" role="alert">  
                <ul>
                    @foreach($errors->all() as $message)
                        <li>{{$message}}</li>	
                    @endforeach
                <ul>	
            </div>
            
            @endif
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('Nome') !!}
                            {!! Form::text('name', $teacher->name , ['required', 'class' => 'form-control', 'placeholder'=>'Nome do professor']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input text">
                            {!! Form::label('E-mail') !!}
                            {!! Form::text('email', $teacher->email, ['required', 'class' => 'form-control', 'placeholder'=>'Digite o e-mail. Ex: exemplo@exemplo.com']) !!}
                        </div>
                    </div>
                </div>
              
              

              
            </div>

            <div class="form-group text-right">
                {!! Form::submit('Atualizar', ['class' => 'btn btn-primary bgpersonalizado']) !!}
            </div>
        </div>

    {!! Form::close() !!}

</div>

@endsection